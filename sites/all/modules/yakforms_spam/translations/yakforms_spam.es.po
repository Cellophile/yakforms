# $Id$
#
# LANGUAGE translation of Drupal (sites-all-modules-framaforms_spam)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  sites/all/modules/framaforms_spam/framaforms_spam.module: n/a
#  sites/all/modules/framaforms_spam/framaforms_spam.info: n/a
#
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2020-10-08 16:35+0200\n"
"PO-Revision-Date: 2020-10-28 10:13+0000\n"
"Last-Translator: Maki <makiniqa@riseup.net>\n"
"Language-Team: Spanish <https://weblate.framasoft.org/projects/framaforms/"
"framaforms-spam/es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.1\n"

#: sites/all/modules/framaforms_spam/framaforms_spam.module:19
msgid "Forbidden words"
msgstr "Palabras prohibidas"

#: sites/all/modules/framaforms_spam/framaforms_spam.module:21
msgid "Words that are considered suspicious when found in new form titles. You can separate forms with commas (,)."
msgstr ""
"Palabras que son consideradas como sospechosas al estar presentes en títulos "
"de nuevos formularios. Puedes separar la palabras con comas (,)."

#: sites/all/modules/framaforms_spam/framaforms_spam.module:26
msgid "Submit"
msgstr "Enviar"

#: sites/all/modules/framaforms_spam/framaforms_spam.module:103
msgid "Your form contains suspicious words and is therefore considered as spam. The site's administrators will review it shortly."
msgstr ""
"Tu formulario contiene palabras sospechosas y ha sido marcada como spam. Les "
"administradores del sitio lo inspeccionarán a la brevedad."

#: sites/all/modules/framaforms_spam/framaforms_spam.module:307
msgid "Forms in quarantine"
msgstr "Formularios en cuarentena"

#: sites/all/modules/framaforms_spam/framaforms_spam.module:308
msgid "Records exposed to Views."
msgstr ""

#: sites/all/modules/framaforms_spam/framaforms_spam.module:318
msgid "Nids of the forms."
msgstr ""

#: sites/all/modules/framaforms_spam/framaforms_spam.module:327
msgid "node"
msgstr "nodo"

#: sites/all/modules/framaforms_spam/framaforms_spam.module:337
msgid "Uid for the author of this node"
msgstr "Uid de le autore de este nodo"

#: sites/all/modules/framaforms_spam/framaforms_spam.module:351
msgid "Date the form was put into quarantine."
msgstr "Fecha de acuarentenamiento del formulario."

#: sites/all/modules/framaforms_spam/framaforms_spam.module:376
msgid "Put forms into quarantine"
msgstr "Poner formularios en cuarentena"

#: sites/all/modules/framaforms_spam/framaforms_spam.module:385
msgid "Take forms out of quarantine"
msgstr "Sacar formularios de cuarentena"

#: sites/all/modules/framaforms_spam/framaforms_spam.module:404
msgid "The form was put into quarantine. You can set it back up using <a href='/quarantine'>this view.</a>"
msgstr ""

#: sites/all/modules/framaforms_spam/framaforms_spam.module:416
msgid "The form was taken out of quarantine and reassigned to user %id"
msgstr "El formulario fue sacado de cuarentena y reasignado a le usuarie %id"

#: sites/all/modules/framaforms_spam/framaforms_spam.module:48;81;149;173;187;198;224;241;258;268
msgid "framaforms_spam"
msgstr "framaforms_spam"

#: sites/all/modules/framaforms_spam/framaforms_spam.module:48
msgid "Launching cron task."
msgstr ""

#: sites/all/modules/framaforms_spam/framaforms_spam.module:81
msgid "Error deleting potential spam : %error"
msgstr "Error de supresión de spam potencial: %error"

#: sites/all/modules/framaforms_spam/framaforms_spam.module:149
msgid "Error checking framaforms_spam_quarantine table : %error"
msgstr "Error al chequear la tabla framaforms_spam_quarantine : %error"

#: sites/all/modules/framaforms_spam/framaforms_spam.module:173
msgid "Error checking framaforms_spam_allowlist table : %error"
msgstr "Error al chequear la tabla framaforms_spam_allowlist : %error"

#: sites/all/modules/framaforms_spam/framaforms_spam.module:187
msgid "Trying to put into quarantine unexisting form %nid"
msgstr "Intento de poner en cuarentena formulario inexistente %nid"

#: sites/all/modules/framaforms_spam/framaforms_spam.module:198
msgid "Error trying to put insert node into allowlist table :  %error"
msgstr "Error al intentar insertar un nodo en la tabla allowlist : %error"

#: sites/all/modules/framaforms_spam/framaforms_spam.module:224
msgid "Error trying to put insert node into quarantine table : %error"
msgstr "Intento de poner en cuarentena formulario inexistente %id"

#: sites/all/modules/framaforms_spam/framaforms_spam.module:241
msgid "Trying to take out of quarantine unexisting form %nid"
msgstr "Intento de sacar de cuarentena formulario inexistente %nid"

#: sites/all/modules/framaforms_spam/framaforms_spam.module:258
msgid "Error trying to take form out of quarantine : %error"
msgstr "Intento de sacar de cuarentena formulario inexistente %error"

#: sites/all/modules/framaforms_spam/framaforms_spam.module:268
msgid "Error trying to delete node from quarantine table : %e"
msgstr "Error al intentar borrar nodo de la tabla de cuarentena: %e"

#: sites/all/modules/framaforms_spam/framaforms_spam.module:6
msgid "Configure Yakforms Spam Protection"
msgstr "Configurar protección de spam de Yakforms"

#: sites/all/modules/framaforms_spam/framaforms_spam.module:7
msgid "Configuration for Yakforms Spam Protection"
msgstr "Configuración de protección de spam de Yakforms"

#: sites/all/modules/framaforms_spam/framaforms_spam.info:0
msgid "Yakforms Spam Protection"
msgstr "Protección de spam de Yakforms"

#: sites/all/modules/framaforms_spam/framaforms_spam.info:0
msgid "Features for automatically detect and quarantine spam forms in Yakforms."
msgstr ""
"Funciones que permiten detectar y poner en cuarentena los formularios spam "
"de Yakforms automáticamente."
